package br.ufrn.imd.dominio

class ContaCorrente {

	int numero
	double saldoAtual
	double saldoAnterior
	Date dataAbertura
	
	
	def exibirDados() {
		"N�mero: $numero, Saldo Atual: $saldoAtual"
	}
	
	def saque(valor) {
		saldoAnterior = saldoAtual
		saldoAtual-=valor
	}
	
	def deposito(valor) {
		saldoAnterior = saldoAtual
		saldoAtual+=valor
	}
		
	def transferencia(ContaCorrente c, valor) {
		saque(valor)
		c.deposito(valor)
	}
			
}

