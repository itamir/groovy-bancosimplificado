import br.ufrn.imd.dominio.Banco
import br.ufrn.imd.dominio.ContaCorrente
import br.ufrn.imd.dominio.Pessoa


def banco = new Banco(codigo: "1", descricao: "Banco do Brasil")

def p1 = new Pessoa(nome: "Aiden")
def p2 = new Pessoa(nome: "Franklin")

def c1 = new ContaCorrente(numero: 111, saldoAtual: 20, 
	saldoAnterior: 0)
def c2 = new ContaCorrente(numero: 222, saldoAtual: 100, 
	saldoAnterior: 1000)
def c3 = new ContaCorrente(numero: 333, saldoAtual: 50, 
	saldoAnterior: 50)

println c1.exibirDados() 
println c2.exibirDados()
println c3.exibirDados()

println c1.deposito(800)
println c3.saque(200)

println c2.transferencia(c3, 300)

banco.codigo = 33
println banco.codigo

println banco.codigoDescricao()

banco.setaCodigo(123232)
println banco.codigo





def c4 = null

println c4?.exibirDados()

c4 = c4?:new ContaCorrente()
c4.numero = 1

println c4.numero

def soma = {a,b -> a + b}

println soma(4,5)

